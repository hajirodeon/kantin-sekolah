# Kantin-Sekolah v1.0

Kantin Sekolah v1.0 untuk Sistem Booking Item Produk dan Kasir Kantin/Minimarket di Sekolah.

Transaksi pembelian hanya bisa dilakukan, bila Siswa/Guru/Staff memiliki Saldo yang cukup untuk berbelanja.




Dibuat dengan Php Native Php 7.4.10 dan Mysql/MariaDB.


Pastikan gunakan Webserver XAMPP PHP 7.4.10 .


---

* Untuk File .SQL bisa import dari Folder /db

* File Konfigurasi di /inc/config.php


---

http://alamat_webnya/admin

Login Admin, Silahkan gunakan User : admin dan Password : admin



---

TAMPILAN HALAMAN WEB KANTIN SEKOLAH :

<img src="tmp_tampilan/image_01.png" width="100%">


<img src="tmp_tampilan/image_02.png" width="100%">


<img src="tmp_tampilan/image_03.png" width="100%">


<img src="tmp_tampilan/image_04.png" width="100%">


<img src="tmp_tampilan/image_05.png" width="100%">


<img src="tmp_tampilan/image_06.png" width="100%">


<img src="tmp_tampilan/image_07.png" width="100%">


<img src="tmp_tampilan/image_08.png" width="100%">


<img src="tmp_tampilan/image_09.png" width="100%">


<img src="tmp_tampilan/image_10.png" width="100%">


<img src="tmp_tampilan/image_11.png" width="100%">


<img src="tmp_tampilan/image_12.png" width="100%">


<img src="tmp_tampilan/image_13.png" width="100%">


<img src="tmp_tampilan/image_14.png" width="100%">


<img src="tmp_tampilan/image_15.png" width="100%">


<img src="tmp_tampilan/image_16.png" width="100%">






---


Bila ada kesulitan atau hambatan atau ingin request custom konten berbayar, 

silahkan bisa kontak via email : hajirodeon@gmail.com 

atau WA : 081-829-88-54










