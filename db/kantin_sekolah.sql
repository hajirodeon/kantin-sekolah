-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 24, 2023 at 04:58 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kantin_sekolah`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminx`
--

CREATE TABLE `adminx` (
  `kd` varchar(50) NOT NULL,
  `usernamex` varchar(100) NOT NULL,
  `passwordx` varchar(100) NOT NULL,
  `postdate` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `adminx`
--

INSERT INTO `adminx` (`kd`, `usernamex`, `passwordx`, `postdate`) VALUES
('1234567890', 'admin', '21232f297a57a5a743894a0e4a801fc3', '2013-02-16 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `member_order`
--

CREATE TABLE `member_order` (
  `kd` varchar(50) NOT NULL DEFAULT '',
  `booking_kode` varchar(100) DEFAULT NULL,
  `booking_postdate` datetime DEFAULT NULL,
  `bayar_status` enum('true','false') NOT NULL DEFAULT 'false',
  `bayar_postdate` datetime DEFAULT NULL,
  `proses_status` enum('true','false') DEFAULT 'false',
  `proses_postdate` datetime DEFAULT NULL,
  `diterima_status` enum('true','false') NOT NULL DEFAULT 'false',
  `diterima_postdate` datetime DEFAULT NULL,
  `barang_jml_jenis` varchar(10) DEFAULT NULL,
  `barang_qty` varchar(10) DEFAULT NULL,
  `barang_berat` varchar(10) DEFAULT NULL,
  `subtotal` varchar(15) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `member_kd` varchar(50) DEFAULT NULL,
  `member_kode` varchar(100) DEFAULT NULL,
  `member_nama` varchar(100) DEFAULT NULL,
  `member_jabatan` varchar(100) DEFAULT NULL,
  `catatan` varchar(100) DEFAULT NULL,
  `total_bayar` varchar(15) DEFAULT NULL,
  `total_kembali` varchar(15) DEFAULT NULL,
  `postdate_update` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `member_order`
--

INSERT INTO `member_order` (`kd`, `booking_kode`, `booking_postdate`, `bayar_status`, `bayar_postdate`, `proses_status`, `proses_postdate`, `diterima_status`, `diterima_postdate`, `barang_jml_jenis`, `barang_qty`, `barang_berat`, `subtotal`, `postdate`, `member_kd`, `member_kode`, `member_nama`, `member_jabatan`, `catatan`, `total_bayar`, `total_kembali`, `postdate_update`) VALUES
('20230221070507848', '20230221070507848', '2023-02-21 07:05:14', 'true', '2023-02-21 07:14:24', 'true', '2023-02-21 08:04:23', 'true', '2023-02-21 08:11:48', '2', '2', '200', '27000', '2023-02-21 07:05:14', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', NULL, NULL, NULL, NULL),
('20230221072601565', '20230221072601565', '2023-02-21 07:26:09', 'true', '2023-02-21 07:26:27', 'true', '2023-02-21 08:13:49', 'false', NULL, '1', '1', '100', '5000', '2023-02-21 07:26:09', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', NULL, NULL, NULL, NULL),
('20230221075252988', '20230221075252988', '2023-02-21 07:52:58', 'true', '2023-02-21 07:53:10', 'false', NULL, 'false', NULL, '1', '1', '100', '17000', '2023-02-21 07:52:58', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', NULL, NULL, NULL, NULL),
('20230221095833603', '20230221095833603', '2023-02-21 10:13:26', 'true', '2023-02-21 10:15:11', 'false', NULL, 'false', NULL, '1', '1', '100', '10000', '2023-02-21 10:13:26', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', NULL, NULL, NULL, NULL),
('cc3f869802a22e6aaa5cf6c830b1d8e9', '20230222094050', '2023-02-22 09:40:50', 'true', '2023-02-22 13:27:40', 'true', '2023-02-22 13:27:40', 'true', '2023-02-22 13:27:40', '2', '0', NULL, '27000', '2023-02-22 09:40:50', NULL, NULL, '', NULL, NULL, '30000', '3000', '2023-02-22 13:27:40'),
('b8682eaa72b7d8698b396931a7630bef', '20230222110816', '2023-02-22 11:08:16', 'true', '2023-02-22 13:27:30', 'true', '2023-02-22 13:27:30', 'true', '2023-02-22 13:27:30', '1', '1', NULL, '5000', '2023-02-22 11:08:16', NULL, NULL, '', NULL, NULL, '10000', '5000', '2023-02-22 13:27:30'),
('a0b7d501b4600d301c4e5545de96b616', '20230222111444', '2023-02-22 11:14:44', 'true', '2023-02-22 13:27:16', 'true', '2023-02-22 13:27:16', 'true', '2023-02-22 13:27:16', '2', '3', NULL, '8800', '2023-02-22 11:26:16', NULL, NULL, '', NULL, NULL, '10000', '1200', '2023-02-22 13:27:16'),
('b024a2f96ad6e0c53d80775392be102c', '20230222113815', '2023-02-22 11:38:15', 'true', '2023-02-22 13:27:07', 'true', '2023-02-22 13:27:07', 'true', '2023-02-22 13:27:07', '1', '1', NULL, '5000', '2023-02-22 11:38:34', NULL, NULL, 'indra', NULL, NULL, '10000', '5000', '2023-02-22 13:27:07'),
('20230222133323687', '20230222133323687', '2023-02-22 13:33:29', 'true', '2023-02-22 13:33:35', 'false', NULL, 'false', NULL, '1', '1', '100', '17000', '2023-02-22 13:33:29', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', NULL, NULL, NULL, NULL),
('2115e8b8cae40204c825721c9aa51fa8', '20230222140802', '2023-02-22 14:08:02', 'true', '2023-02-22 14:15:24', 'true', '2023-02-22 14:15:24', 'true', '2023-02-22 14:15:24', '2', '0', NULL, '6900', '2023-02-22 14:15:24', NULL, NULL, 'yus', NULL, NULL, '10000', '3100', NULL),
('49d3cc99b49071f2745d45b0cb9a54e4', '20230222141818', '2023-02-22 14:18:18', 'true', '2023-02-22 14:18:41', 'true', '2023-02-22 14:18:41', 'true', '2023-02-22 14:18:41', '2', '0', NULL, '6900', '2023-02-22 14:18:41', NULL, NULL, 'yesi', NULL, NULL, '10000', '3100', NULL),
('e9f5f89126f6b39e68a2e7edb6f9e81a', '20230222142158', '2023-02-22 14:21:58', 'true', '2023-02-22 14:22:14', 'true', '2023-02-22 14:22:14', 'true', '2023-02-22 14:22:14', '1', '1', '100', '5000', '2023-02-22 14:22:14', NULL, NULL, 'siap', NULL, NULL, '100000', '95000', NULL),
('43a048159ef0992c5b8b0f404360d38d', '20230222142430', '2023-02-22 14:24:30', 'true', '2023-02-22 14:25:02', 'true', '2023-02-22 14:25:02', 'true', '2023-02-22 14:25:02', '2', '2', '200', '6900', '2023-02-22 14:25:02', NULL, NULL, 'oo', NULL, NULL, '10000', '3100', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `member_order_detail`
--

CREATE TABLE `member_order_detail` (
  `kd` varchar(50) NOT NULL,
  `booking_kd` varchar(50) DEFAULT NULL,
  `booking_kode` varchar(100) DEFAULT NULL,
  `member_kd` varchar(50) DEFAULT NULL,
  `member_kode` varchar(100) DEFAULT NULL,
  `member_nama` varchar(100) DEFAULT NULL,
  `member_jabatan` varchar(100) DEFAULT NULL,
  `item_kd` varchar(50) DEFAULT NULL,
  `item_filex1` longtext DEFAULT NULL,
  `item_kode` varchar(100) DEFAULT NULL,
  `item_nama` varchar(100) DEFAULT NULL,
  `item_berat` varchar(10) DEFAULT NULL,
  `item_harga` varchar(15) DEFAULT NULL,
  `item_kategori` varchar(100) DEFAULT NULL,
  `item_satuan` varchar(100) DEFAULT NULL,
  `item_kondisi` varchar(10) DEFAULT NULL,
  `jumlah` varchar(10) DEFAULT NULL,
  `subtotal` varchar(15) DEFAULT NULL,
  `subtotal_berat` varchar(10) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `member_order_detail`
--

INSERT INTO `member_order_detail` (`kd`, `booking_kd`, `booking_kode`, `member_kd`, `member_kode`, `member_nama`, `member_jabatan`, `item_kd`, `item_filex1`, `item_kode`, `item_nama`, `item_berat`, `item_harga`, `item_kategori`, `item_satuan`, `item_kondisi`, `jumlah`, `subtotal`, `subtotal_berat`, `postdate`) VALUES
('8e20288fe25418ff11d946b2d73a0d5a', '20230221070507848', '20230221070507848', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', '3dfd595650cd69fdb6adb367786c20ef', '3dfd595650cd69fdb6adb367786c20efxstrix1.jpg', NULL, 'produk #03', '100', '17000', NULL, NULL, 'BARU', '1', '17000', '100', '2023-02-21 07:08:25'),
('cdc527a2896e87064c83467e0d3c4367', '20230221070507848', '20230221070507848', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', '05d75cc9626470b433dd184c18ac42aa', '05d75cc9626470b433dd184c18ac42aaxstrix1.jpg', NULL, 'produk #02', '100', '10000', NULL, NULL, 'BARU', '1', '10000', '100', '2023-02-21 07:13:52'),
('c94097df1d08d283cd5e8cc378195e2a', '20230221072601565', '20230221072601565', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', 'b547f716f17cc33ecbe4ff56c88a66ef', 'b547f716f17cc33ecbe4ff56c88a66efxstrix1.jpg', NULL, 'produk #1', '100', '5000', NULL, NULL, 'BARU', '1', '5000', '100', '2023-02-21 07:26:09'),
('5e0436b24d8449300f815753698ba8fc', '20230221075252988', '20230221075252988', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', '3dfd595650cd69fdb6adb367786c20ef', '3dfd595650cd69fdb6adb367786c20efxstrix1.jpg', NULL, 'produk #03', '100', '17000', NULL, NULL, 'BARU', '1', '17000', '100', '2023-02-21 07:52:58'),
('df5120902c46a416702edeb83b9cd27b', '20230221095833603', '20230221095833603', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', '05d75cc9626470b433dd184c18ac42aa', '05d75cc9626470b433dd184c18ac42aaxstrix1.jpg', NULL, 'produk #02', '100', '10000', NULL, NULL, 'BARU', '1', '10000', '100', '2023-02-21 10:13:26'),
('9723545257c1ac9bd70b5df1b760f7e3', 'cc3f869802a22e6aaa5cf6c830b1d8e9', NULL, NULL, NULL, NULL, NULL, '3dfd595650cd69fdb6adb367786c20ef', NULL, 'IP003', 'produk #03', NULL, '17000', NULL, NULL, NULL, '1', '17000', NULL, '2023-02-22 09:45:39'),
('423fceb294c98f6087aeb0d40e0608eb', 'cc3f869802a22e6aaa5cf6c830b1d8e9', NULL, NULL, NULL, NULL, NULL, '05d75cc9626470b433dd184c18ac42aa', NULL, 'IP002', 'produk #02', NULL, '10000', NULL, NULL, NULL, '1', '10000', NULL, '2023-02-22 09:49:07'),
('3a31595c77e77089acac935ee5838bac', 'b8682eaa72b7d8698b396931a7630bef', NULL, NULL, NULL, NULL, NULL, 'b547f716f17cc33ecbe4ff56c88a66ef', NULL, '23001', 'produk #1', NULL, '5000', NULL, NULL, NULL, '1', '5000', NULL, '2023-02-22 11:08:26'),
('bc9891bbc234cb9edc5db54d382b80ac', 'a0b7d501b4600d301c4e5545de96b616', NULL, NULL, NULL, NULL, NULL, 'b547f716f17cc33ecbe4ff56c88a66ef', NULL, '23001', 'produk #1', NULL, '5000', 'Gorengan', 'PCS', NULL, '1', '5000', NULL, '2023-02-22 11:20:36'),
('645097e2f96d4ed7f949f54db694b6ac', 'a0b7d501b4600d301c4e5545de96b616', NULL, NULL, NULL, NULL, NULL, '87c5082f1a391f55e15caa24bb06ec91', NULL, '23911', '23911', NULL, '1900', 'Makanan', 'PCS', NULL, '2', '3800', NULL, '2023-02-22 11:20:25'),
('a3f4c8c8dfb2e02a62d0536506b6c298', 'b024a2f96ad6e0c53d80775392be102c', NULL, NULL, NULL, NULL, NULL, 'b547f716f17cc33ecbe4ff56c88a66ef', NULL, '23001', 'produk #1', NULL, '5000', 'Gorengan', 'PCS', NULL, '1', '5000', NULL, '2023-02-22 11:38:26'),
('596406315848c52db9cdd8c68b75302d', '20230222133323687', '20230222133323687', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', '3dfd595650cd69fdb6adb367786c20ef', '3dfd595650cd69fdb6adb367786c20efxstrix1.jpg', NULL, 'produk #03', '100', '17000', NULL, NULL, 'BARU', '1', '17000', '100', '2023-02-22 13:33:29'),
('a65fc041ffeabe14d3892ab8e866e9aa', '2115e8b8cae40204c825721c9aa51fa8', NULL, NULL, NULL, NULL, NULL, '87c5082f1a391f55e15caa24bb06ec91', NULL, '23911', '23911', '100', '1900', 'Makanan', 'PCS', 'BARU', '1', '1900', '100', '2023-02-22 14:12:13'),
('864f71f09fd9bd69030bb33f22724eea', '2115e8b8cae40204c825721c9aa51fa8', NULL, NULL, NULL, NULL, NULL, 'b547f716f17cc33ecbe4ff56c88a66ef', NULL, '23001', 'produk #1', '100', '5000', 'Gorengan', 'PCS', 'BARU', '1', '5000', '100', '2023-02-22 14:12:28'),
('5a8a540c580f9fb3f9dba127c250fe68', '49d3cc99b49071f2745d45b0cb9a54e4', NULL, NULL, NULL, NULL, NULL, 'b547f716f17cc33ecbe4ff56c88a66ef', NULL, '23001', 'produk #1', '100', '5000', 'Gorengan', 'PCS', 'BARU', '1', '5000', '100', '2023-02-22 14:18:25'),
('a405e65e2ce7b5a4fecb5c1adc899f94', '49d3cc99b49071f2745d45b0cb9a54e4', NULL, NULL, NULL, NULL, NULL, '87c5082f1a391f55e15caa24bb06ec91', NULL, '23911', '23911', '100', '1900', 'Makanan', 'PCS', 'BARU', '1', '1900', '100', '2023-02-22 14:18:30'),
('c5966820318ec3ba967fcdc07b14c59b', 'e9f5f89126f6b39e68a2e7edb6f9e81a', NULL, NULL, NULL, NULL, NULL, 'b547f716f17cc33ecbe4ff56c88a66ef', NULL, '23001', 'produk #1', '100', '5000', 'Gorengan', 'PCS', 'BARU', '1', '5000', '100', '2023-02-22 14:22:04'),
('1d44cafd9d109573cf713e74e6a55ce8', '43a048159ef0992c5b8b0f404360d38d', NULL, NULL, NULL, NULL, NULL, 'b547f716f17cc33ecbe4ff56c88a66ef', 'b547f716f17cc33ecbe4ff56c88a66efxstrix1.jpg', '23001', 'produk #1', '100', '5000', 'Gorengan', 'PCS', 'BARU', '1', '5000', '100', '2023-02-22 14:24:37'),
('dbb808b9ed6ef070af40d33bae64aeae', '43a048159ef0992c5b8b0f404360d38d', NULL, NULL, NULL, NULL, NULL, '87c5082f1a391f55e15caa24bb06ec91', '87c5082f1a391f55e15caa24bb06ec91xstrix1.jpg', '23911', '23911', '100', '1900', 'Makanan', 'PCS', 'BARU', '1', '1900', '100', '2023-02-22 14:24:43');

-- --------------------------------------------------------

--
-- Table structure for table `member_saldo`
--

CREATE TABLE `member_saldo` (
  `kd` varchar(50) NOT NULL,
  `member_kd` varchar(50) DEFAULT NULL,
  `member_kode` varchar(100) DEFAULT NULL,
  `member_nama` varchar(100) DEFAULT NULL,
  `member_jabatan` varchar(100) DEFAULT NULL,
  `tglnya` date DEFAULT NULL,
  `nominal` varchar(15) DEFAULT NULL,
  `jenis` varchar(100) DEFAULT NULL,
  `ket` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `member_saldo`
--

INSERT INTO `member_saldo` (`kd`, `member_kd`, `member_kode`, `member_nama`, `member_jabatan`, `tglnya`, `nominal`, `jenis`, `ket`, `postdate`) VALUES
('63d81243740bee7edad63a8937a25f12', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', '2023-02-19', '90000', 'DEBET', '100000', '2023-02-19 10:53:30'),
('74a5796a8cba994aea84e2e846119966', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', '2023-02-19', '500000', 'DEBET', 'ok ya...', '2023-02-19 17:05:00'),
('c2a831e482b23fe77b06316e5b2ac644', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', '2023-02-20', '10000', 'KREDIT', '-', '2023-02-20 17:58:00'),
('a6d4a03c5100a68155f15063e1679148', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', '2023-02-21', '27000', 'KREDIT', '-', '2023-02-21 07:14:24'),
('434678887e004a7e58ad260deb4375a4', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', '2023-02-21', '5000', 'KREDIT', '-', '2023-02-21 07:26:27'),
('4399ada4aacb2606bf95737246d4ca7c', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', '2023-02-21', '17000', 'KREDIT', '-', '2023-02-21 07:53:10'),
('69c52baa4a97ac5b28cff694c64f110d', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', '2023-02-21', '10000', 'KREDIT', '-', '2023-02-21 10:15:11'),
('1f94bb22d5d5d6ea6cb1cc1a82813730', 'f1beaf277a0ba2efdec5ed4ce8f5435b', '123', 'agus', 'Siswa', '2023-02-22', '17000', 'KREDIT', 'Pembelian untuk Transaksi : 20230222133323687. Rp 17.000,00', '2023-02-22 13:33:35');

-- --------------------------------------------------------

--
-- Table structure for table `m_info`
--

CREATE TABLE `m_info` (
  `kd` varchar(50) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `isi` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `jml_dilihat` varchar(10) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_info`
--

INSERT INTO `m_info` (`kd`, `judul`, `isi`, `postdate`, `jml_dilihat`) VALUES
('5a545eb9a96d6549b04967573c2f8f93', 'info 1', 'isi info 1', '2023-02-18 10:07:17', '4'),
('520a26a83eb317438fb8a20354facd70', 'info 2', 'isi info2', '2023-02-18 10:07:36', '3');

-- --------------------------------------------------------

--
-- Table structure for table `m_item`
--

CREATE TABLE `m_item` (
  `kd` varchar(50) NOT NULL DEFAULT '',
  `kode` varchar(15) DEFAULT NULL,
  `barkode` varchar(100) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `isi` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `filex1` longtext DEFAULT NULL,
  `filex2` longtext DEFAULT NULL,
  `filex3` longtext DEFAULT NULL,
  `filex4` longtext DEFAULT NULL,
  `filex5` longtext DEFAULT NULL,
  `jml` varchar(10) DEFAULT NULL,
  `jml_min` varchar(10) DEFAULT NULL,
  `harga` varchar(15) DEFAULT NULL,
  `berat` varchar(10) DEFAULT NULL,
  `kategori` varchar(100) DEFAULT NULL,
  `jml_dilihat` varchar(10) DEFAULT '0',
  `jml_terjual` varchar(10) DEFAULT '0',
  `kondisi` varchar(10) DEFAULT NULL,
  `url_cantik` longtext DEFAULT NULL,
  `satuan` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_item`
--

INSERT INTO `m_item` (`kd`, `kode`, `barkode`, `nama`, `isi`, `postdate`, `filex1`, `filex2`, `filex3`, `filex4`, `filex5`, `jml`, `jml_min`, `harga`, `berat`, `kategori`, `jml_dilihat`, `jml_terjual`, `kondisi`, `url_cantik`, `satuan`) VALUES
('b547f716f17cc33ecbe4ff56c88a66ef', '23001', '123456723001', 'produk #1', 'isinya', '2023-02-22 11:05:43', 'b547f716f17cc33ecbe4ff56c88a66ef-1.jpg', 'b547f716f17cc33ecbe4ff56c88a66ef-1.jpg', 'b547f716f17cc33ecbe4ff56c88a66ef-1.jpg', 'b547f716f17cc33ecbe4ff56c88a66ef-1.jpg', 'b547f716f17cc33ecbe4ff56c88a66ef-1.jpg', '83', '1', '5000', '100', 'Gorengan', '75', '8', 'BARU', 'produk++1', 'PCS'),
('05d75cc9626470b433dd184c18ac42aa', '23002', '123456723002', 'produk #02', 'isi produknya...', '2023-02-22 11:05:00', '05d75cc9626470b433dd184c18ac42aa-1.jpg', '05d75cc9626470b433dd184c18ac42aa-1.jpg', '05d75cc9626470b433dd184c18ac42aa-1.jpg', '05d75cc9626470b433dd184c18ac42aa-1.jpg', '05d75cc9626470b433dd184c18ac42aa-1.jpg', '50', '10', '10000', '100', 'Roti Basah', '89', '3', 'BARU', 'produk++02', 'PCS'),
('3dfd595650cd69fdb6adb367786c20ef', '23003', '123456723003', 'produk #03', 'isi produk 3', '2023-02-22 11:05:28', '3dfd595650cd69fdb6adb367786c20ef-1.jpg', '3dfd595650cd69fdb6adb367786c20ef-1.jpg', '3dfd595650cd69fdb6adb367786c20ef-1.jpg', '3dfd595650cd69fdb6adb367786c20ef-1.jpg', '3dfd595650cd69fdb6adb367786c20ef-1.jpg', '33', '10', '17000', '100', 'Kerupuk', '80', '4', 'BARU', 'produk++03', 'PCS'),
('87c5082f1a391f55e15caa24bb06ec91', '23911', '123456723911', 'Gorengan', 'isi Gorengan', '2023-02-22 11:04:46', '87c5082f1a391f55e15caa24bb06ec91-1.jpg', '87c5082f1a391f55e15caa24bb06ec91-1.jpg', '87c5082f1a391f55e15caa24bb06ec91-1.jpg', '87c5082f1a391f55e15caa24bb06ec91-1.jpg', '87c5082f1a391f55e15caa24bb06ec91-1.jpg', '3', '2', '1900', '100', 'Makanan', '1', '5', 'BARU', 'gorengan', 'PCS');

-- --------------------------------------------------------

--
-- Table structure for table `m_kategori`
--

CREATE TABLE `m_kategori` (
  `kd` varchar(50) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_kategori`
--

INSERT INTO `m_kategori` (`kd`, `nama`, `postdate`) VALUES
('60101c18b9b2c9577ba04a261e635c90', 'Gorengan', NULL),
('ca8f49bb13e5ce44089532eb4dac02a0', 'Roti Kering', NULL),
('4d0b67667f00f0dd34cb9557ca775550', 'Roti Basah', NULL),
('3062a18010cee54a02f0417010d904f2', 'Snack', NULL),
('8e1fc6af9e877d74f1b21c96c1ca1d87', 'Kerupuk', NULL),
('53e344b4279a37399da7a33f16a35dae', 'Makanan', NULL),
('006e64ec743f8f26d469b76b1e1378ea', 'Minuman', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_member`
--

CREATE TABLE `m_member` (
  `kd` varchar(50) NOT NULL,
  `usernamex` varchar(100) DEFAULT NULL,
  `passwordx` varchar(100) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `telp` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_member`
--

INSERT INTO `m_member` (`kd`, `usernamex`, `passwordx`, `kode`, `nama`, `jabatan`, `telp`, `postdate`) VALUES
('f1beaf277a0ba2efdec5ed4ce8f5435b', '123', '202cb962ac59075b964b07152d234b70', '123', 'agus', 'Siswa', '123', '2023-02-18 09:59:43'),
('8ef5765554f1bb2658afa6bc5544d7b6', '234', '289dff07669d7a23de0ef88d2f7129e7', '234', '234', 'Siswa', '234', '2023-02-18 10:00:14'),
('945efae261c3ea375b65426b0dbecec3', '345', 'd81f9c1be2e08964bf9f24b15f0e4900', '345', '345', 'Pegawai', '345', '2023-02-18 10:00:25');

-- --------------------------------------------------------

--
-- Table structure for table `m_reputasi`
--

CREATE TABLE `m_reputasi` (
  `kd` varchar(50) NOT NULL,
  `jml_sukses` varchar(10) DEFAULT NULL,
  `jml_transaksi` varchar(10) DEFAULT NULL,
  `jml_member` varchar(10) DEFAULT NULL,
  `login_terakhir` datetime DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `jml_item` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `m_satuan`
--

CREATE TABLE `m_satuan` (
  `kd` varchar(50) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_satuan`
--

INSERT INTO `m_satuan` (`kd`, `nama`, `postdate`) VALUES
('bd7a33d5864b6b187f58db105b20890c', 'PCS', '2023-02-22 09:53:50'),
('709660a124ac54a8166385e6eb7d8b32', 'DUS', '2023-02-22 09:53:54'),
('776ef8d7784b30fd9540346d1bcf5d25', 'LUSIN', '2023-02-22 09:54:12');

-- --------------------------------------------------------

--
-- Table structure for table `user_log_entri`
--

CREATE TABLE `user_log_entri` (
  `kd` varchar(50) NOT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `user_jabatan` varchar(100) DEFAULT NULL,
  `ket` longtext DEFAULT NULL,
  `dibaca` enum('true','false') NOT NULL DEFAULT 'false',
  `dibaca_postdate` datetime DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_log_entri`
--

INSERT INTO `user_log_entri` (`kd`, `user_kd`, `user_kode`, `user_nama`, `user_jabatan`, `ket`, `dibaca`, `dibaca_postdate`, `postdate`) VALUES
('9e732ce8f1932d1dd839a98234ef54c8', '1234567890', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Selamat Datang, ADMIN.  [Administrator]', 'true', '2023-02-24 10:56:52', '2023-02-24 10:56:38'),
('dd6e3e914ceda68d5f894c8ea3a7fc3a', '1234567890', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [HISTORY]. History Entri', 'true', '2023-02-24 10:56:52', '2023-02-24 10:56:41'),
('281fe5c887f981e6699f26fe3338791a', '1234567890', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Selamat Datang, ADMIN.  [Administrator]', 'true', '2023-02-24 10:56:52', '2023-02-24 10:56:47'),
('114b9e77232e743ba31326521eb8bf5d', '1234567890', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING]. Ganti Password', 'true', '2023-02-24 10:56:52', '2023-02-24 10:56:50'),
('9849b3c2eb623c6e0e44686c70c0598c', '1234567890', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [HISTORY]. History Entri', 'false', NULL, '2023-02-24 10:56:52'),
('a5694ab34892cc85300b9eceb8be647a', '1234567890', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Selamat Datang, ADMIN.  [Administrator]', 'false', NULL, '2023-02-24 10:56:55');

-- --------------------------------------------------------

--
-- Table structure for table `user_log_login`
--

CREATE TABLE `user_log_login` (
  `kd` varchar(50) NOT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `user_jabatan` varchar(100) DEFAULT NULL,
  `ipnya` varchar(100) DEFAULT NULL,
  `dibaca` enum('true','false') NOT NULL DEFAULT 'false',
  `dibaca_postdate` datetime DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_log_login`
--

INSERT INTO `user_log_login` (`kd`, `user_kd`, `user_kode`, `user_nama`, `user_jabatan`, `ipnya`, `dibaca`, `dibaca_postdate`, `postdate`) VALUES
('15905e2e38983d0a4357147414d043d8', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', '127.0.0.1', 'true', '2023-02-24 10:53:24', '2023-02-24 10:53:17'),
('584bb15db9c70ab30ef59776fa580f79', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', '127.0.0.1', 'false', NULL, '2023-02-24 10:56:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminx`
--
ALTER TABLE `adminx`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `member_order`
--
ALTER TABLE `member_order`
  ADD PRIMARY KEY (`kd`),
  ADD KEY `kd` (`kd`),
  ADD KEY `no_nota` (`booking_kode`);
ALTER TABLE `member_order` ADD FULLTEXT KEY `no_nota_2` (`booking_kode`);
ALTER TABLE `member_order` ADD FULLTEXT KEY `barang_jml_jenis` (`barang_jml_jenis`);
ALTER TABLE `member_order` ADD FULLTEXT KEY `barang_berat` (`barang_berat`);
ALTER TABLE `member_order` ADD FULLTEXT KEY `subtotal` (`subtotal`);
ALTER TABLE `member_order` ADD FULLTEXT KEY `member_nama` (`member_kode`);

--
-- Indexes for table `member_order_detail`
--
ALTER TABLE `member_order_detail`
  ADD PRIMARY KEY (`kd`),
  ADD KEY `kd` (`kd`),
  ADD KEY `member_order_kd` (`booking_kd`);
ALTER TABLE `member_order_detail` ADD FULLTEXT KEY `member_nota` (`booking_kode`);
ALTER TABLE `member_order_detail` ADD FULLTEXT KEY `member_nama` (`member_nama`);
ALTER TABLE `member_order_detail` ADD FULLTEXT KEY `item_kd` (`item_kd`);
ALTER TABLE `member_order_detail` ADD FULLTEXT KEY `item_nama` (`item_nama`);
ALTER TABLE `member_order_detail` ADD FULLTEXT KEY `item_berat` (`item_berat`);
ALTER TABLE `member_order_detail` ADD FULLTEXT KEY `item_harga` (`item_harga`);
ALTER TABLE `member_order_detail` ADD FULLTEXT KEY `item_kondisi` (`item_kondisi`);
ALTER TABLE `member_order_detail` ADD FULLTEXT KEY `jumlah` (`jumlah`);
ALTER TABLE `member_order_detail` ADD FULLTEXT KEY `subtotal` (`subtotal`);

--
-- Indexes for table `member_saldo`
--
ALTER TABLE `member_saldo`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `m_info`
--
ALTER TABLE `m_info`
  ADD PRIMARY KEY (`kd`),
  ADD KEY `kd` (`kd`);
ALTER TABLE `m_info` ADD FULLTEXT KEY `judul` (`judul`);
ALTER TABLE `m_info` ADD FULLTEXT KEY `isi` (`isi`);

--
-- Indexes for table `m_item`
--
ALTER TABLE `m_item`
  ADD PRIMARY KEY (`kd`),
  ADD KEY `kd` (`kd`);
ALTER TABLE `m_item` ADD FULLTEXT KEY `kode` (`kode`);
ALTER TABLE `m_item` ADD FULLTEXT KEY `nama` (`nama`);
ALTER TABLE `m_item` ADD FULLTEXT KEY `isi` (`isi`);
ALTER TABLE `m_item` ADD FULLTEXT KEY `jml` (`jml`);
ALTER TABLE `m_item` ADD FULLTEXT KEY `jml_min` (`jml_min`);
ALTER TABLE `m_item` ADD FULLTEXT KEY `harga` (`harga`);
ALTER TABLE `m_item` ADD FULLTEXT KEY `berat` (`berat`);
ALTER TABLE `m_item` ADD FULLTEXT KEY `kategori` (`kategori`);
ALTER TABLE `m_item` ADD FULLTEXT KEY `jml_dilihat` (`jml_dilihat`);
ALTER TABLE `m_item` ADD FULLTEXT KEY `jml_terjual` (`jml_terjual`);
ALTER TABLE `m_item` ADD FULLTEXT KEY `kondisi` (`kondisi`);
ALTER TABLE `m_item` ADD FULLTEXT KEY `url_cantik` (`url_cantik`);

--
-- Indexes for table `m_kategori`
--
ALTER TABLE `m_kategori`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `m_member`
--
ALTER TABLE `m_member`
  ADD PRIMARY KEY (`kd`),
  ADD KEY `kd` (`kd`);
ALTER TABLE `m_member` ADD FULLTEXT KEY `usernamex` (`usernamex`);
ALTER TABLE `m_member` ADD FULLTEXT KEY `nama` (`nama`);
ALTER TABLE `m_member` ADD FULLTEXT KEY `email` (`jabatan`);

--
-- Indexes for table `m_reputasi`
--
ALTER TABLE `m_reputasi`
  ADD PRIMARY KEY (`kd`),
  ADD KEY `kd` (`kd`);

--
-- Indexes for table `m_satuan`
--
ALTER TABLE `m_satuan`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `user_log_entri`
--
ALTER TABLE `user_log_entri`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `user_log_login`
--
ALTER TABLE `user_log_login`
  ADD PRIMARY KEY (`kd`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
