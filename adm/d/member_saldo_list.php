<?php
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
/////// KANTIN-SEKOLAH v1.0                                     ///////
///////////////////////////////////////////////////////////////////////
/////// Dibuat oleh :                                           ///////
/////// Agus Muhajir, S.Kom                                     ///////
/////// URL 	:                                               ///////
///////     * http://github.com/hajirodeon/                     ///////
///////     * http://sisfokol.wordpress.com/                    ///////
///////     * http://hajirodeon.wordpress.com/                  ///////
///////     * http://yahoogroup.com/groups/sisfokol/            ///////
///////     * https://www.youtube.com/@hajirodeon               ///////
/////// E-Mail	:                                               ///////
///////     * hajirodeon@yahoo.com                              ///////
///////     * hajirodeon@gmail.com                              ///////
/////// HP/SMS/WA : 081-829-88-54                               ///////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////


session_start();

require("../../inc/config.php");
require("../../inc/fungsi.php");
require("../../inc/koneksi.php");
require("../../inc/cek/adm.php");
require("../../inc/class/paging.php");
$tpl = LoadTpl("../../template/admin.html");

nocache;

//nilai
$filenya = "member_saldo_list.php";
$judul = "[MEMBER] History Saldo Member";
$judulku = "$judul";
$judulx = $judul;
$kd = nosql($_REQUEST['kd']);
$s = nosql($_REQUEST['s']);
$kunci = cegah($_REQUEST['kunci']);
$kunci2 = balikin($_REQUEST['kunci']);
$page = nosql($_REQUEST['page']);
if ((empty($page)) OR ($page == "0"))
	{
	$page = "1";
	}



$limit = 5;


//PROSES ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//jika export
//export
if ($_POST['btnEX'])
	{
	//require
	require('../../inc/class/excel/OLEwriter.php');
	require('../../inc/class/excel/BIFFwriter.php');
	require('../../inc/class/excel/worksheet.php');
	require('../../inc/class/excel/workbook.php');


	
	//nama file e...
	$i_filename = "saldo_member_list.xls";
	$i_judul = "member_saldo_list";
	
	
	
	
	//header file
	function HeaderingExcel($i_filename)
		{
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition:attachment;filename=$i_filename");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Pragma: public");
		}
	
	
	
	
	//bikin...
	HeaderingExcel($i_filename);
	$workbook = new Workbook("-");
	$worksheet1 =& $workbook->add_worksheet($i_judul);
	$worksheet1->write_string(0,0,"NO.");
	$worksheet1->write_string(0,1,"POSTDATE");
	$worksheet1->write_string(0,2,"KODE");
	$worksheet1->write_string(0,3,"NAMA");
	$worksheet1->write_string(0,4,"JABATAN");
	$worksheet1->write_string(0,5,"DEBET");
	$worksheet1->write_string(0,6,"KREDIT");
	$worksheet1->write_string(0,7,"KETERANGAN");
	
	
	//data
	$qdt = mysqli_query($koneksi, "SELECT * FROM member_saldo ".
										"ORDER BY postdate DESC");
	$rdt = mysqli_fetch_assoc($qdt);
	
	do
		{
		//nilai
		$dt_nox = $dt_nox + 1;
		$i_postdate = balikin($rdt['postdate']);
		$i_m_kode = balikin($rdt['member_kode']);
		$i_m_nama = balikin($rdt['member_nama']);
		$i_m_jabatan = balikin($rdt['member_jabatan']);
		$i_jenis = balikin($rdt['jenis']);
		$i_nominal = balikin($rdt['nominal']);
		$i_ket = balikin($rdt['ket']);
	
	
	
		//jika debet
		if ($i_jenis == "DEBET")
			{
			$i_nil_debet = xduit3($i_nominal);
			}
	
	
		//jika kredit
		if ($i_jenis == "KREDIT")
			{
			$i_nil_kredit = xduit3($i_nominal);
			}
	
	
	
		//ciptakan
		$worksheet1->write_string($dt_nox,0,$dt_nox);
		$worksheet1->write_string($dt_nox,1,$i_postdate);
		$worksheet1->write_string($dt_nox,2,$i_m_kode);
		$worksheet1->write_string($dt_nox,3,$i_m_nama);
		$worksheet1->write_string($dt_nox,4,$i_m_jabatan);
		$worksheet1->write_string($dt_nox,5,$i_nil_debet);
		$worksheet1->write_string($dt_nox,6,$i_nil_kredit);
		$worksheet1->write_string($dt_nox,7,$i_ket);
		}
	while ($rdt = mysqli_fetch_assoc($qdt));


	//close
	$workbook->close();

	
	
	//re-direct
	xloc($filenya);
	exit();
	}








//nek batal
if ($_POST['btnBTL'])
	{
	//re-direct
	xloc($filenya);
	exit();
	}





//jika cari
if ($_POST['btnCARI'])
	{
	//nilai
	$kunci = cegah($_POST['kunci']);


	//re-direct
	$ke = "$filenya?kunci=$kunci";
	xloc($ke);
	exit();
	}




//nek entri baru
if ($_POST['btnBARU'])
	{
	//re-direct
	$ke = "member_saldo.php";
	xloc($ke);
	exit();
	}





//jika hapus
if ($_POST['btnHPS'])
	{
	//ambil nilai
	$jml = nosql($_POST['jml']);
	$page = nosql($_POST['page']);
	$ke = "$filenya?page=$page";

	//ambil semua
	for ($i=1; $i<=$jml;$i++)
		{
		//ambil nilai
		$yuk = "item";
		$yuhu = "$yuk$i";
		$kd = nosql($_POST["$yuhu"]);

		//del
		mysqli_query($koneksi, "DELETE FROM member_saldo ".
									"WHERE kd = '$kd'");
		}

	//auto-kembali
	xloc($filenya);
	exit();
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//isi *START
ob_start();


//require
require("../../template/js/jumpmenu.js");
require("../../template/js/checkall.js");
require("../../template/js/swap.js");
?>


  
  <script>
  	$(document).ready(function() {
    $('#table-responsive').dataTable( {
        "scrollX": true
    } );
} );
  </script>
  
<?php
//view //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//jika null
if (empty($kunci))
	{
	$sqlcount = "SELECT * FROM member_saldo ".
					"ORDER BY postdate DESC";
	}
	
else
	{
	$sqlcount = "SELECT * FROM member_saldo ".
					"WHERE member_kode LIKE '%$kunci%' ".
					"OR member_nama LIKE '%$kunci%' ".
					"OR member_jabatan LIKE '%$kunci%' ".
					"OR jenis LIKE '%$kunci%' ".
					"OR nominal LIKE '%$kunci%' ".
					"OR ket LIKE '%$kunci%' ".
					"OR postdate LIKE '%$kunci%' ".
					"ORDER BY postdate DESC";
	}
	

//query
$p = new Pager();
$start = $p->findStart($limit);

$sqlresult = $sqlcount;

$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
$pages = $p->findPages($count, $limit);
$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
$pagelist = $p->pageList($_GET['page'], $pages, $target);
$data = mysqli_fetch_array($result);



echo '<form action="'.$filenya.'" method="post" name="formxx">
<p>
<input name="btnBARU" type="submit" value="<< ENTRI SALDO" class="btn btn-danger">
<input name="btnEX" type="submit" value="EXPORT EXCEL >>" class="btn btn-success">
</p>
<hr>

</form>



<form action="'.$filenya.'" method="post" name="formx">
<p>
<input name="kunci" type="text" value="'.$kunci2.'" size="20" class="btn btn-warning" placeholder="Kata Kunci...">
<input name="btnCARI" type="submit" value="CARI" class="btn btn-danger">
<input name="btnBTL" type="submit" value="RESET" class="btn btn-info">
</p>
	

<div class="table-responsive">          
<table class="table" border="1">
<thead>

<tr valign="top" bgcolor="'.$warnaheader.'">
<td width="20">&nbsp;</td>
<td width="50"><strong><font color="'.$warnatext.'">POSTDATE</font></strong></td>
<td width="100" align="center"><strong><font color="'.$warnatext.'">NIS/KODE</font></strong></td>
<td width="200" align="center"><strong><font color="'.$warnatext.'">NAMA</font></strong></td>
<td width="100" align="center"><strong><font color="'.$warnatext.'">JABATAN</font></strong></td>
<td width="150" align="center"><strong><font color="'.$warnatext.'">DEBET</font></strong></td>
<td width="150" align="center"><strong><font color="'.$warnatext.'">KREDIT</font></strong></td>
<td align="center"><strong><font color="'.$warnatext.'">KETERANGAN</font></strong></td>
</tr>
</thead>
<tbody>';

if ($count != 0)
	{
	do 
		{
		if ($warna_set ==0)
			{
			$warna = $warna01;
			$warna_set = 1;
			}
		else
			{
			$warna = $warna02;
			$warna_set = 0;
			}

		$nomer = $nomer + 1;
		$i_kd = nosql($data['kd']);
		$i_postdate = balikin($data['postdate']);
		$i_m_kode = balikin($data['member_kode']);
		$i_m_nama = balikin($data['member_nama']);
		$i_m_jabatan = balikin($data['member_jabatan']);
		$i_jenis = balikin($data['jenis']);
		$i_nominal = balikin($data['nominal']);
		$i_ket = balikin($data['ket']);


	
		//jika debet
		if ($i_jenis == "DEBET")
			{
			$i_nil_debet = xduit3($i_nominal);
			}
		else
			{
			$i_nil_debet = "-";
			}
	
	
		//jika kredit
		if ($i_jenis == "KREDIT")
			{
			$i_nil_kredit = xduit3($i_nominal);
			}
		else
			{
			$i_nil_kredit = "-";
			}
	
	
	
	
		
		echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
		echo '<td>
		<input type="checkbox" name="item'.$nomer.'" value="'.$i_kd.'">
        </td>
		<td>'.$i_postdate.'</td>
		<td>'.$i_m_kode.'</td>
		<td>'.$i_m_nama.'</td>
		<td>'.$i_m_jabatan.'</td>
		<td align="right">'.$i_nil_debet.'</td>
		<td align="right">'.$i_nil_kredit.'</td>
		<td>'.$i_ket.'</td>
        </tr>';
		}
	while ($data = mysqli_fetch_assoc($result));
	}


echo '</tbody>
  </table>
  </div>


<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr>
<td>
<strong><font color="#FF0000">'.$count.'</font></strong> Data. '.$pagelist.'
<br>
<br>

<input name="jml" type="hidden" value="'.$count.'">
<input name="s" type="hidden" value="'.$s.'">
<input name="kd" type="hidden" value="'.$kdx.'">
<input name="page" type="hidden" value="'.$page.'">

<input name="btnALL" type="button" value="SEMUA" onClick="checkAll('.$count.')" class="btn btn-primary">
<input name="btnBTL" type="reset" value="BATAL" class="btn btn-warning">
<input name="btnHPS" type="submit" value="HAPUS" class="btn btn-danger">
</td>
</tr>
</table>
</form>';






//isi
$isi = ob_get_contents();
ob_end_clean();

require("../../inc/niltpl.php");


//null-kan
xclose($koneksi);
exit();
?>