<?php
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
/////// KANTIN-SEKOLAH v1.0                                     ///////
///////////////////////////////////////////////////////////////////////
/////// Dibuat oleh :                                           ///////
/////// Agus Muhajir, S.Kom                                     ///////
/////// URL 	:                                               ///////
///////     * http://github.com/hajirodeon/                     ///////
///////     * http://sisfokol.wordpress.com/                    ///////
///////     * http://hajirodeon.wordpress.com/                  ///////
///////     * http://yahoogroup.com/groups/sisfokol/            ///////
///////     * https://www.youtube.com/@hajirodeon               ///////
/////// E-Mail	:                                               ///////
///////     * hajirodeon@yahoo.com                              ///////
///////     * hajirodeon@gmail.com                              ///////
/////// HP/SMS/WA : 081-829-88-54                               ///////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////


session_start();

//ambil nilai
require("../inc/config.php");
require("../inc/fungsi.php");
require("../inc/koneksi.php");
require("../inc/cek/adm.php");
require("../inc/class/paging.php");
$tpl = LoadTpl("../template/admin.html");


nocache;

//nilai
$filenya = "index.php";
$judul = "Selamat Datang, ADMIN.";
$judulku = "$judul  [$adm_session]";





//isi *START
ob_start();







//query
$qtyk = mysqli_query($koneksi, "SELECT * FROM m_item");
$rtyk = mysqli_fetch_assoc($qtyk);
$jml_item = mysqli_num_rows($qtyk);



//query
$qtyk = mysqli_query($koneksi, "SELECT * FROM m_member");
$rtyk = mysqli_fetch_assoc($qtyk);
$jml_member = mysqli_num_rows($qtyk);






//query
$qtyk = mysqli_query($koneksi, "SELECT SUM(jml) AS total ".
						"FROM m_item");
$rtyk = mysqli_fetch_assoc($qtyk);
$jml_stock = nosql($rtyk['total']);



//query
$qtyk = mysqli_query($koneksi, "SELECT * FROM member_order");
$rtyk = mysqli_fetch_assoc($qtyk);
$jml_order = mysqli_num_rows($qtyk);





//query
$qtyk = mysqli_query($koneksi, "SELECT SUM(subtotal) AS total ".
									"FROM member_order ".
									"WHERE diterima_status = 'true'");
$rtyk = mysqli_fetch_assoc($qtyk);
$jml_omzet = nosql($rtyk['total']);









//query
$qtyk = mysqli_query($koneksi, "SELECT * FROM member_order ".
									"WHERE bayar_status = 'true' ".
									"AND proses_status = 'false'");
$rtyk = mysqli_fetch_assoc($qtyk);
$jml_p_baru = mysqli_num_rows($qtyk);





//query
$qtyk = mysqli_query($koneksi, "SELECT * FROM member_order ".
									"WHERE bayar_status = 'true' ".
									"AND proses_status = 'true' ".
									"AND diterima_status = 'false'");
$rtyk = mysqli_fetch_assoc($qtyk);
$jml_p_proses = mysqli_num_rows($qtyk);








//query
$qtyk = mysqli_query($koneksi, "SELECT * FROM member_order ".
									"WHERE bayar_status = 'true' ".
									"AND diterima_status = 'true'");
$rtyk = mysqli_fetch_assoc($qtyk);
$jml_p_selesai = mysqli_num_rows($qtyk);







			
//ketahui sisa saldo
//total kredit	
$qku = mysqli_query($koneksi, "SELECT SUM(nominal) AS totalnya ".
								"FROM member_saldo ".
								"WHERE jenis = 'KREDIT'");
$rku = mysqli_fetch_assoc($qku);
$saldo_kreditnya = balikin($rku['totalnya']);


//total debet
$qku = mysqli_query($koneksi, "SELECT SUM(nominal) AS totalnya ".
								"FROM member_saldo ".
								"WHERE jenis = 'DEBET'");
$rku = mysqli_fetch_assoc($qku);
$saldo_debetnya = balikin($rku['totalnya']);
	
	
$saldo_sisa = $saldo_debetnya - $saldo_kreditnya;





?>

      <!-- Info boxes -->
      <div class="row">

        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-archive"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">ITEM PRODUK</span>
              <span class="info-box-number"><?php echo $jml_item;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->



        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-briefcase"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">STOCK</span>
              <span class="info-box-number"><?php echo $jml_stock;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        





        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-cyan"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">MEMBER</span>
              <span class="info-box-number"><?php echo $jml_member;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        





        



                
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-opencart"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">TOTAL TRANSAKSI</span>
              <span class="info-box-number"><?php echo $jml_order;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        
        
		<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fa fa-play"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">PESANAN BARU</span>
              <span class="info-box-number"><?php echo $jml_p_baru;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>




		<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fa fa-spinner"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">PROSES PESANAN</span>
              <span class="info-box-number"><?php echo $jml_p_proses;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        

		<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fa fa-stop-circle"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">SELESAI</span>
              <span class="info-box-number"><?php echo $jml_p_selesai;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>





		<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-money"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">OMZET</span>
              <span class="info-box-number"><?php echo xduit3($jml_omzet);?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>



		<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">SISA SALDO</span>
              <span class="info-box-number"><?php echo xduit3($saldo_sisa);?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>



		<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">TOTAL DEBET</span>
              <span class="info-box-number"><?php echo xduit3($saldo_debetnya);?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        
        
		<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">TOTAL KREDIT</span>
              <span class="info-box-number"><?php echo xduit3($saldo_kreditnya);?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>









        <!-- /.col -->
      </div>
      <!-- /.row -->



      <div class="row">
        <div class="col-md-12">


			<?php
			//query
			$p = new Pager();
			$limit = 5;
			$start = $p->findStart($limit);
	
			$sqlcount = "SELECT * FROM member_order ".
							"WHERE proses_status = 'false' ".
							"ORDER BY booking_postdate DESC";
			$sqlresult = $sqlcount;
	
			$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
			$pages = $p->findPages($count, $limit);
			$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
			$pagelist = $p->pageList($_GET['page'], $pages, $target);
			$data = mysqli_fetch_array($result);

				
						
			if ($count != 0)
				{
				//total nominal
				$qtyk = mysqli_query($koneksi, "SELECT SUM(subtotal) AS totalnya ".
													"FROM member_order ".
													"WHERE proses_status = 'false'");
				$rtyk = mysqli_fetch_assoc($qtyk);
				$e_totalnya = nosql($rtyk['totalnya']);
							
							
						
					
				//view data				  
				echo '<br>
				[<b><font color="green">'.$count.'</font></b> Pesanan Baru]. 
				[Total Nominal : <b><font color="green">'.xduit3($e_totalnya).'</font></b>].
				<div class="table-responsive">          
				  <table class="table" border="1">
				    <thead>
						<tr bgcolor="'.$warnaheader.'">
						<th>BOOKING</th>
				        <th>MEMBER</th>
				        <th>ITEM</th>
				        <th>TOTAL ITEM</th>
				        <th>TOTAL QTY</th>
				        <th>TOTAL BERAT</th>
				        <th>TOTAL BAYAR</th>
				      </tr>
				    </thead>
				    <tbody>';
		
		
		
				do
					{
					if ($warna_set ==0)
						{
						$warna = $warna01;
						$warna_set = 1;
						}
					else
						{
						$warna = $warna02;
						$warna_set = 0;
						}
		
					//nilai
					$nomer = $nomer + 1;
					$kd = nosql($data['kd']);
					$i_member_kd = nosql($data['member_kd']);
					$i_member_nama = balikin($data['member_nama']);
					$i_nota = balikin($data['booking_kode']);
					$i_jml_jenis = balikin($data['barang_jml_jenis']);
					$i_jml_qty = balikin($data['barang_qty']);
					$i_jml_berat = balikin($data['barang_berat']);
					$i_member_nama = balikin($data['member_nama']);
					$i_booking = balikin($data['booking_postdate']);
					$i_tgl_bayar = balikin($data['bayar_postdate']);
					$i_total = balikin($data['subtotal']);
		
		
		
					echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
					echo '<td>
					<p>
					'.$i_tgl_bayar.'
					</p>
		
					<br>
					Kode Booking :
					<br>
					'.$i_nota.'
					
					<br>
					<a href="'.$filenya.'?s=proses&kd='.$kd.'" class="btn btn-block btn-danger">CEK DAN PROSES</a>
					</td>
					<td>'.$i_member_nama.'</td>
					<td>';
					
		
					//query
					$q = mysqli_query($koneksi, "SELECT * FROM member_order_detail ".
													"WHERE member_kd = '$i_member_kd' ".
													"AND booking_kd = '$kd' ".
													"ORDER BY item_nama ASC");
					$row = mysqli_fetch_assoc($q);
					$total = mysqli_num_rows($q);
				
		
					echo '<table class="table" border="1">
							<tr bgcolor="'.$warnaheader.'">
				          <td>NAMA</td>
				          <td>KONDISI</td>
				          <td>BERAT</td>
				          <td>JUMLAH</td>
				        </tr>';
				
				
				
						do 
							{
							$r_kd = nosql($row['kd']);
							$r_itemkd = nosql($row['item_kd']);
							$r_nama = balikin($row['item_nama']);
							$r_kondisi = balikin($row['item_kondisi']);
							$r_berat = balikin($row['item_berat']);
							$r_filex = balikin($row['item_filex1']);
							$r_harga = nosql($row['item_harga']);
							$r_qty = nosql($row['jumlah']);
							$r_subtotal = nosql($row['subtotal']);
								
							if ($warna_set ==0)
								{
								$warna = $warna01;
								$warna_set = 1;
								}
							else
								{
								$warna = $warna02;
								$warna_set = 0;
								}
					
									
								//stock yang ada
								$qtyk = mysqli_query($koneksi, "SELECT * FROM m_item ".
																	"WHERE kd = '$r_itemkd'");
								$rtyk = mysqli_fetch_assoc($qtyk);
								$e_jml = nosql($rtyk['jml']);
								
						
								echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
								echo '<td width="150">
								<img src="'.$sumber.'/filebox/item/'.$r_itemkd.'/'.$r_filex.'" width="150">
								<br>
								'.$r_nama.'
								<hr>
								'.xduit3($r_harga).'
								</td>
								<td width="100">'.$r_kondisi.'</td>
								<td width="100">'.$r_berat.' gram</td>
								<td width="50">'.$r_qty.'</td>
						        </tr>';
								}
							while ($row = mysqli_fetch_assoc($q));
					
							echo '</table>';
		
		
					echo '</td>
					<td>'.$i_jml_jenis.'</td>
					<td>'.$i_jml_qty.'</td>
					<td>'.$i_jml_berat.' Gram</td>
					<td>'.xduit3($i_total).'</td>
			    	</tr>';
					}
				while ($data = mysqli_fetch_assoc($result));
		
				echo '</tbody>
				  </table>
				  </div>';
				  }
			?>

		</div>
	</div>






      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">TRANSAKSI SEMINGGU INI...</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">



				
				

				  <script>
				  	$(document).ready(function() {
				    $('#table-responsive').dataTable( {
				        "scrollX": true
				    } );
				} );
				  </script>
				  

				<?php
				echo '<div class="table-responsive">          
					  <table class="table" border="1">
					    <thead>
							
				<tr bgcolor="'.$warnaheader.'">';
				
				echo '<td width="75" align="center"></td>';
													
													
				//tanggal sekarang
				$m = date("m");
				$de = date("d");
				$y = date("Y");
				
				//ambil 7hari terakhir
				for($i=0; $i<=7; $i++)
					{
					$nilku = date('Y-m-d',mktime(0,0,0,$m,($de-$i),$y)); 
				
					echo '<td width="75" align="center"><strong><font color="'.$warnatext.'">'.$nilku.'</font></strong></td>';
					}


				echo '</tr>
				
				<tr>
				<td width="75" align="center">Baru</td>';
								
				//tanggal sekarang
				$m = date("m");
				$de = date("d");
				$y = date("Y");
				
				//ambil 7hari terakhir
				for($i=0; $i<=7; $i++)
					{
					$nilku = date('Y-m-d',mktime(0,0,0,$m,($de-$i),$y)); 
				
				
					//ketahui ordernya...
					$qyuk = mysqli_query($koneksi, "SELECT * FROM member_order ".
														"WHERE bayar_postdate LIKE '$nilku%' ".
														"AND bayar_status = 'true'");
					$tyuk = mysqli_num_rows($qyuk);
					
					if (empty($tyuk))
						{
						$tyuk = "";
						}
					
					
					
					echo '<td width="75" align="center">
					'.$tyuk.'					
					</td>';
					}


				echo '</tr>
				
				<tr>
				<td width="75" align="center">Proses</td>';
								
				//tanggal sekarang
				$m = date("m");
				$de = date("d");
				$y = date("Y");
				
				//ambil 7hari terakhir
				for($i=0; $i<=7; $i++)
					{
					$nilku = date('Y-m-d',mktime(0,0,0,$m,($de-$i),$y)); 
				
				
					//ketahui ordernya...
					$qyuk = mysqli_query($koneksi, "SELECT * FROM member_order ".
														"WHERE proses_postdate LIKE '$nilku%' ".
														"AND proses_status = 'true' ".
														"AND diterima_status = 'false'");
					$tyuk = mysqli_num_rows($qyuk);
					
					
					if (empty($tyuk))
						{
						$tyuk = "";
						}
					
					
					
					echo '<td width="75" align="center">
					'.$tyuk.'					
					</td>';
					}


				echo '</tr>
				
				
				<tr>
				<td width="75" align="center">Selesai</td>';
								
				//tanggal sekarang
				$m = date("m");
				$de = date("d");
				$y = date("Y");
				
				//ambil 7hari terakhir
				for($i=0; $i<=7; $i++)
					{
					$nilku = date('Y-m-d',mktime(0,0,0,$m,($de-$i),$y)); 
				
				
					//ketahui ordernya...
					$qyuk = mysqli_query($koneksi, "SELECT * FROM member_order ".
														"WHERE diterima_postdate LIKE '$nilku%' ".
														"AND diterima_status = 'true'");
					$tyuk = mysqli_num_rows($qyuk);
					
					
					if (empty($tyuk))
						{
						$tyuk = "";
						}
					
					
					
					echo '<td width="75" align="center">
					'.$tyuk.'					
					</td>';
					}


				echo '</tr>
				</tbody>
				  </table>
				  </div>
				  <hr>
				  <br>
				  <br>';



				?>
				
				
				    
				    

                </div>
                
                <!-- /.col -->
                <div class="col-md-4">
                	
                	
                	<?php
		            	echo '<div class="card card-danger">
			              <div class="card-header">
			                <h3 class="card-title">ITEM TERLARIS</h3>
			
			                <div class="card-tools">
			                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
			                  </button>
			                </div>
		
			              </div>
		
			              <div class="card-body">';
		

							//daftar item laris
							$qyuk = mysqli_query($koneksi, "SELECT * FROM m_item ".
																"ORDER BY round(jml_terjual) DESC LIMIT 0,10");
							$ryuk = mysqli_fetch_assoc($qyuk);
							
							do
								{
								$yuk_itemkd = nosql($ryuk['kd']);
								$yuk_jml = nosql($ryuk['jml_terjual']);
								$yuk_nama = balikin($ryuk['nama']);
			
			
								//dapatkan jumlahnya
								$qku2 = mysqli_query($koneksi, "SELECT SUM(jumlah) AS totalnya ".
																	"FROM member_order_detail ".
																	"WHERE item_kd = '$yuk_itemkd'");
								$rku2 = mysqli_fetch_assoc($qku2);
								$ku2_totalnya = balikin($rku2['totalnya']);
								
								
								//update kan
								mysqli_query($koneksi, "UPDATE m_item SET jml_terjual = '$ku2_totalnya' ".
															"WHERE kd = '$yuk_itemkd'");
								
							
							
								$nilaiku = round(($ku2_totalnya / 100) * 100);	
								?>
				                  <!-- /.progress-group -->
				                  <div class="progress-group">
				                    <span class="progress-text"><?php echo $yuk_nama;?> [<?php echo $ku2_totalnya?>]</span>
				
				                    <div class="progress sm">
				                      <div class="progress-bar progress-bar-yellow" style="width: <?php echo $nilaiku;?>%"></div>
				                    </div>
				                  </div>
				                <?php
								}
							while ($ryuk = mysqli_fetch_assoc($qyuk));
							?>  
			
										
		
		
			              </div>
			            </div>




                  <!-- /.progress-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->



<?php




//isi
$isi = ob_get_contents();
ob_end_clean();

require("../inc/niltpl.php");

//diskonek
xfree($qbw);
xclose($koneksi);
exit();
?>