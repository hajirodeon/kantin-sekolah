<?php
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
/////// KANTIN-SEKOLAH v1.0                                     ///////
///////////////////////////////////////////////////////////////////////
/////// Dibuat oleh :                                           ///////
/////// Agus Muhajir, S.Kom                                     ///////
/////// URL 	:                                               ///////
///////     * http://github.com/hajirodeon/                     ///////
///////     * http://sisfokol.wordpress.com/                    ///////
///////     * http://hajirodeon.wordpress.com/                  ///////
///////     * http://yahoogroup.com/groups/sisfokol/            ///////
///////     * https://www.youtube.com/@hajirodeon               ///////
/////// E-Mail	:                                               ///////
///////     * hajirodeon@yahoo.com                              ///////
///////     * hajirodeon@gmail.com                              ///////
/////// HP/SMS/WA : 081-829-88-54                               ///////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////


session_start();

//fungsi - fungsi
require("../../inc/config.php");
require("../../inc/fungsi.php");
require("../../inc/koneksi.php");
$tpl = LoadTpl("../../template/window.html");


nocache;

//nilai
$judulku = "Nota";
$notakd = nosql($_REQUEST['notakd']);


//print re-direct
$diload = "window.print();location.href='nota.php';";





//isi *START
ob_start();


//query
$q = mysqli_query($koneksi, "SELECT * FROM member_order ".
								"WHERE kd = '$notakd'");
$r = mysqli_fetch_assoc($q);
$total = mysqli_num_rows($q);
$no_nota = nosql($r['booking_kode']);
$tot_total = nosql($r['total']);
$tot_bayar = nosql($r['total_bayar']);
$tot_kembali = nosql($r['total_kembali']);


//header
echo '<table width="200" border="0" cellspacing="0" cellpadding="3">
<tr valign="top">
<td align="center">
<big><strong>'.$nama_toko.'</strong></big>
<br>
'.$alamat_toko.'
</td>
</tr>
</table>';

//tgl & no nota
echo '<table width="200" border="0" cellspacing="0" cellpadding="3">
<tr valign="top">
<td>
<hr>
'.$no_nota.'
<br>
<hr>
</td>
</tr>
</table>';

echo '<table width="200" border="0" cellspacing="0" cellpadding="3">
<tr valign="top">
<td align="center">
<hr>
</td>
</tr>
</table>';



//kolom header
echo '<table width="200" border="0" cellspacing="0" cellpadding="3">';


//row kolom data
$qx = mysqli_query($koneksi, "SELECT member_order_detail.*, member_order_detail.qty AS kdqty, ".
									"member_order_detail.subtotal AS kdsub ".
									"FROM member_order_detail ".
									"WHERE booking_kd = '$notakd' ".
									"ORDER BY item_nama ASC");
$rqx = mysqli_fetch_assoc($qx);

do
	{
	$nomer = $nomer + 1;
	$mbkod = nosql($rqx['item_kode']);
	$mbnm = balikin($rqx['item_nama']);

	//nek akeh...
	if (strlen($mbnm) > 15)
		{
		$mbnm1 = substr(balikin($rqx['mbnm']),0,15);
		$mbnmx = "$mbnm1...";
		}
	else
		{
		$mbnmx = $mbnm;
		}

	$kdqty = nosql($rqx['jumlah']);
	$satuan = balikin($rqx['item_satuan']);
	$x_qty = "$kdqty $satuan";
	$kdsub = nosql($rqx['kdsub']);
	$hrg_jual = nosql($rqx['item_harga']);

	echo '<tr>
	<td>
	'.$mbnm.'...
	<br>
	'.$x_qty.' * '.$hrg_jual.'
	<br>
	<strong>'.xduit2($kdsub).'</strong>';
	

		echo '<table width="200" border="0" cellspacing="0" cellpadding="3">
		<tr valign="top">
		<td align="center">
		&nbsp;
		</td>
		</tr>
		</table>';
		
	
	
	echo '</td>
	</tr>';
	}
while ($rqx = mysqli_fetch_assoc($qx));

echo '</table>';


echo '<table width="200" border="0" cellspacing="0" cellpadding="3">
<tr valign="top">
<td align="center">
<hr>
</td>
</tr>
</table>';



//total
echo '<table width="200" border="0" cellspacing="0" cellpadding="3">
<tr valign="top">
<td width="100">
<hr>
<strong>Total : </strong>
<br>
'.xduit2($tot_total).'
<br><br>
<strong>Bayar/Charge : </strong>
<br>
'.xduit2($tot_bayar).'
<br><br>
<strong>Kembalian/Change : </strong>
<br>
'.xduit2($tot_kembali).'
</td>
</tr>
</table>';


echo '<table width="200" border="0" cellspacing="0" cellpadding="3">
<tr valign="top">
<td align="center">
<hr>
</td>
</tr>
</table>';

//petugas
echo '<table width="200" border="0" cellspacing="0" cellpadding="3">
<tr valign="top">
<td width="100">
<hr>
Terima Kasih Telah Berbelanja.
<hr>
</td>
</tr>
</table>';


echo '<table width="200" border="0" cellspacing="0" cellpadding="3">
<tr valign="top">
<td align="center">
&nbsp;
</td>
</tr>
</table>';



echo '<table width="200" border="0" cellspacing="0" cellpadding="3">
<tr valign="top">
<td align="center">
&nbsp;
</td>
</tr>
</table>';




//isi
$isi = ob_get_contents();
ob_end_clean();

require("../../inc/niltpl.php");


//null-kan
xclose($koneksi);
exit();
?>