<?php
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
/////// KANTIN-SEKOLAH v1.0                                     ///////
///////////////////////////////////////////////////////////////////////
/////// Dibuat oleh :                                           ///////
/////// Agus Muhajir, S.Kom                                     ///////
/////// URL 	:                                               ///////
///////     * http://github.com/hajirodeon/                     ///////
///////     * http://sisfokol.wordpress.com/                    ///////
///////     * http://hajirodeon.wordpress.com/                  ///////
///////     * http://yahoogroup.com/groups/sisfokol/            ///////
///////     * https://www.youtube.com/@hajirodeon               ///////
/////// E-Mail	:                                               ///////
///////     * hajirodeon@yahoo.com                              ///////
///////     * hajirodeon@gmail.com                              ///////
/////// HP/SMS/WA : 081-829-88-54                               ///////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////


session_start();

require("../../inc/config.php");
require("../../inc/fungsi.php");
require("../../inc/koneksi.php");
require("../../inc/cek/adm.php");
$tpl = LoadTpl("../../template/admin.html");

nocache;

//nilai
$filenya = "jual_tgl.php";
$judul = "Per Tanggal";
$judul = "[LAPORAN]. Per Tanggal";
$judulku = "$judul";
$judulx = $judul;
$ubln = nosql($_REQUEST['ubln']);
$uthn = nosql($_REQUEST['uthn']);

$s = nosql($_REQUEST['s']);
$kunci = cegah($_REQUEST['kunci']);
$kunci2 = balikin($_REQUEST['kunci']);
$page = nosql($_REQUEST['page']);
if ((empty($page)) OR ($page == "0"))
	{
	$page = "1";
	}



//PROSES ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//nek batal
if ($_POST['btnBTL'])
	{
	//re-direct
	xloc($filenya);
	exit();
	}





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////












//isi *START
ob_start();


//require
require("../../template/js/jumpmenu.js");
require("../../template/js/checkall.js");
require("../../template/js/swap.js");
?>
	
	
	  
  <script>
  	$(document).ready(function() {
    $('#table-responsive').dataTable( {
        "scrollX": true
    } );
} );
  </script>
  
<?php
//view //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


echo '<form action="'.$filenya.'" method="post" name="formx">
<div class="row">
	<div class="col-md-12">';
	

	echo "<p>
	Bulan : 
	<br>
	<select name=\"ublnx\" onChange=\"MM_jumpMenu('self',this,0)\" class=\"btn btn-warning\">";
	echo '<option value="'.$ubln.'" selected>'.$arrbln[$ubln].'</option>';
	
	for ($i=1;$i<=12;$i++)
		{
		echo '<option value="'.$filenya.'?ubln='.$i.'">'.$arrbln[$i].'</option>';
		}
				
	echo '</select>';
	


	echo "<select name=\"ublnx\" onChange=\"MM_jumpMenu('self',this,0)\" class=\"btn btn-warning\">";
	echo '<option value="'.$uthn.'" selected>'.$uthn.'</option>';
	
	for ($i=$tahun;$i<=$tahun+1;$i++)
		{
		echo '<option value="'.$filenya.'?ubln='.$ubln.'&uthn='.$i.'">'.$i.'</option>';
		}
				
	echo '</select>
	</p>		
	
	
	
	</div>
</div>

<hr>';


if (empty($ubln))
	{
	echo '<font color="red">
	<h3>BULAN Belum Dipilih...!!</h3>
	</font>';
	}


else if (empty($uthn))
	{
	echo '<font color="red">
	<h3>TAHUN Belum Dipilih...!!</h3>
	</font>';
	}
	
else
	{
	$month = round($ubln);
	$year = round($uthn);
	
	//tanggal terakhir  
	$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
		
	
	echo '<input name="ubln" type="hidden" value="'.$ubln.'">
	<input name="uthn" type="hidden" value="'.$uthn.'">
	
	<div class="table-responsive">          
	<table class="table" border="1">
	<thead>
	
	<tr valign="top" bgcolor="'.$warnaheader.'">
	<td width="100"><strong><font color="'.$warnatext.'">TANGGAL</font></strong></td>
	<td><strong><font color="'.$warnatext.'">JUMLAH MEMBER</font></strong></td>
	<td><strong><font color="'.$warnatext.'">JUMLAH ITEM</font></strong></td>
	<td><strong><font color="'.$warnatext.'">JUMLAH QTY</font></strong></td>
	<td><strong><font color="'.$warnatext.'">SUBTOTAL</font></strong></td>
	</tr>
	</thead>
	<tbody>';
	
	for ($k=1;$k<=$days_in_month;$k++) 
		{
		if ($warna_set ==0)
			{
			$warna = $warna01;
			$warna_set = 1;
			}
		else
			{
			$warna = $warna02;
			$warna_set = 0;
			}


		
		//jumlahnya
		$qku = mysqli_query($koneksi, "SELECT DISTINCT(member_kd) AS totalnya ".
										"FROM member_order ".
										"WHERE round(DATE_FORMAT(postdate, '%d')) = '$k' ".
										"AND round(DATE_FORMAT(postdate, '%m')) = '$ubln' ".
										"AND round(DATE_FORMAT(postdate, '%Y')) = '$uthn'");
		$rku = mysqli_fetch_assoc($qku);
		$t_member = mysqli_num_rows($qku);
		
		
		
		//jumlahnya
		$qku = mysqli_query($koneksi, "SELECT DISTINCT(barang_jml_jenis) AS totalnya ".
										"FROM member_order ".
										"WHERE round(DATE_FORMAT(postdate, '%d')) = '$k' ".
										"AND round(DATE_FORMAT(postdate, '%m')) = '$ubln' ".
										"AND round(DATE_FORMAT(postdate, '%Y')) = '$uthn'");
		$rku = mysqli_fetch_assoc($qku);
		$t_jml = mysqli_num_rows($qku);
		
		
		
		
		//qty
		$qku = mysqli_query($koneksi, "SELECT DISTINCT(barang_qty) AS totalnya ".
										"FROM member_order ".
										"WHERE round(DATE_FORMAT(postdate, '%d')) = '$k' ".
										"AND round(DATE_FORMAT(postdate, '%m')) = '$ubln' ".
										"AND round(DATE_FORMAT(postdate, '%Y')) = '$uthn'");
		$rku = mysqli_fetch_assoc($qku);
		$t_qty = mysqli_num_rows($qku);
		
		
		//qty
		$qku = mysqli_query($koneksi, "SELECT DISTINCT(subtotal) AS totalnya ".
										"FROM member_order ".
										"WHERE round(DATE_FORMAT(postdate, '%d')) = '$k' ".
										"AND round(DATE_FORMAT(postdate, '%m')) = '$ubln' ".
										"AND round(DATE_FORMAT(postdate, '%Y')) = '$uthn'");
		$rku = mysqli_fetch_assoc($qku);
		$t_subtotal = mysqli_num_rows($qku);
		
		

		echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
		echo '<td>'.$k.'.</td>
		<td><font color="red">'.$t_member.'</font></td>
		<td><font color="red">'.$t_jml.'</font></td>
		<td><font color="red">'.$t_qty.'</font></td>
		<td><font color="red">'.$t_subtotal.'</font></td>
		</tr>';
		}
	



	//jumlahnya
	$qku = mysqli_query($koneksi, "SELECT DISTINCT(member_kd) AS totalnya ".
									"FROM member_order ".
									"WHERE round(DATE_FORMAT(postdate, '%m')) = '$ubln' ".
									"AND round(DATE_FORMAT(postdate, '%Y')) = '$uthn'");
	$rku = mysqli_fetch_assoc($qku);
	$t_member = mysqli_num_rows($qku);
	
	
	
	//jumlahnya
	$qku = mysqli_query($koneksi, "SELECT DISTINCT(barang_jml_jenis) AS totalnya ".
									"FROM member_order ".
									"WHERE round(DATE_FORMAT(postdate, '%m')) = '$ubln' ".
									"AND round(DATE_FORMAT(postdate, '%Y')) = '$uthn'");
	$rku = mysqli_fetch_assoc($qku);
	$t_jml = mysqli_num_rows($qku);
	
	
	
	
	//qty
	$qku = mysqli_query($koneksi, "SELECT DISTINCT(barang_qty) AS totalnya ".
									"FROM member_order ".
									"WHERE round(DATE_FORMAT(postdate, '%m')) = '$ubln' ".
									"AND round(DATE_FORMAT(postdate, '%Y')) = '$uthn'");
	$rku = mysqli_fetch_assoc($qku);
	$t_qty = mysqli_num_rows($qku);
	
	
	//qty
	$qku = mysqli_query($koneksi, "SELECT DISTINCT(subtotal) AS totalnya ".
									"FROM member_order ".
									"WHERE round(DATE_FORMAT(postdate, '%m')) = '$ubln' ".
									"AND round(DATE_FORMAT(postdate, '%Y')) = '$uthn'");
	$rku = mysqli_fetch_assoc($qku);
	$t_subtotal = mysqli_num_rows($qku);
	

	echo '<tr valign="top" bgcolor="'.$warnaheader.'">
	<td>&nbsp;</td>
	<td><strong><font color="'.$warnatext.'">'.$t_member.'</font></strong></td>
	<td><strong><font color="'.$warnatext.'">'.$t_jml.'</font></strong></td>
	<td><strong><font color="'.$warnatext.'">'.$t_qty.'</font></strong></td>
	<td><strong><font color="'.$warnatext.'">'.$t_subtotal.'</font></strong></td>
	</tr>
	
	
	</tbody>
	  </table>
	  </div>';

	}


echo '</form>';



//isi
$isi = ob_get_contents();
ob_end_clean();

require("../../inc/niltpl.php");


//null-kan
xclose($koneksi);
exit();
?>