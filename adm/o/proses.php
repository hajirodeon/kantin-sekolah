<?php
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
/////// KANTIN-SEKOLAH v1.0                                     ///////
///////////////////////////////////////////////////////////////////////
/////// Dibuat oleh :                                           ///////
/////// Agus Muhajir, S.Kom                                     ///////
/////// URL 	:                                               ///////
///////     * http://github.com/hajirodeon/                     ///////
///////     * http://sisfokol.wordpress.com/                    ///////
///////     * http://hajirodeon.wordpress.com/                  ///////
///////     * http://yahoogroup.com/groups/sisfokol/            ///////
///////     * https://www.youtube.com/@hajirodeon               ///////
/////// E-Mail	:                                               ///////
///////     * hajirodeon@yahoo.com                              ///////
///////     * hajirodeon@gmail.com                              ///////
/////// HP/SMS/WA : 081-829-88-54                               ///////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////



session_start();

require("../../inc/config.php");
require("../../inc/fungsi.php");
require("../../inc/koneksi.php");
require("../../inc/cek/adm.php");
require("../../inc/class/paging.php");
$tpl = LoadTpl("../../template/admin.html");

nocache;

//nilai
$filenya = "proses.php";
$judul = "[PESANAN] Diproses";
$judulku = $judul;
$judulx = $judul;

$s = nosql($_REQUEST['s']);
$m = nosql($_REQUEST['m']);
$kunci = cegah($_REQUEST['kunci']);
$kd = nosql($_REQUEST['kd']);

$ke = $filenya;
$page = nosql($_REQUEST['page']);
if ((empty($page)) OR ($page == "0"))
	{
	$page = "1";
	}




//PROSES ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//jika selesaikan
if ($s == "selesai")
	{
	mysqli_query($koneksi, "UPDATE member_order SET diterima_status = 'true', ".
								"diterima_postdate = '$today' ".
								"WHERE kd = '$kd'");
					
					
	//re-direct
	xloc($filenya);
	exit();
	}




//reset
if ($_POST['btnRST'])
	{
	//re-direct
	xloc($filenya);
	exit();
	}





//cari
if ($_POST['btnCARI'])
	{
	//nilai
	$kunci = cegah($_POST['kunci']);


	//cek
	if (empty($kunci))
		{
		//re-direct
		$pesan = "Input Pencarian Tidak Lengkap. Harap diperhatikan...!!";
		pekem($pesan,$filenya);
		exit();
		}
	else
		{
		//re-direct
		$ke = "$filenya?kunci=$kunci";
		xloc($ke);
		exit();
		}
	}




//batal
if ($_POST['btnBTL'])
	{
	//re-direct
	xloc($filenya);
	exit();
	}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////






//isi *START
ob_start();




//require
require("../../template/js/jumpmenu.js");
require("../../template/js/checkall.js");
require("../../template/js/number.js");
require("../../template/js/swap.js");

?>


  
  <script>
  	$(document).ready(function() {
    $('#table-responsive').dataTable( {
        "scrollX": true
    } );
} );
  </script>
  
<?php
//view //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo '<form action="'.$filenya.'" enctype="multipart/form-data" method="post" name="formx">
<input name="crkd" type="hidden" value="'.$crkd.'">
<input name="crtipe" type="hidden" value="'.$crtipe.'">
<input name="kd" type="hidden" value="'.$kd.'">
<input name="s" type="hidden" value="'.$s.'">';


echo '<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr>
<td>

<input name="kunci" type="text" value="'.$kunci.'" size="20" class="btn btn-warning">
<input name="btnCARI" type="submit" class="btn btn-danger" value="CARI >>">
<input name="btnRST" type="submit" class="btn btn-info" value="RESET">
</td>
</tr>
</table>';


//jika view /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (empty($s))
	{
	//kunci
	if (!empty($kunci))
		{
		//query
		$p = new Pager();
		$start = $p->findStart($limit);

		$sqlcount = "SELECT * FROM member_order ".
						"WHERE proses_status = 'true' ".
						"AND diterima_status = 'false' ".
						"AND (member_nama LIKE '%$kunci%' ".
						"OR booking_postdate LIKE '%$kunci%' ".
						"OR barang_jml_jenis LIKE '%$kunci%' ".
						"OR barang_qty LIKE '%$kunci%' ".
						"OR barang_berat LIKE '%$kunci%' ".
						"OR subtotal LIKE '%$kunci%' ".
						"OR booking_kode LIKE '%$kunci%') ".
						"ORDER BY booking_postdate DESC";
		$sqlresult = $sqlcount;

		$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
		$pages = $p->findPages($count, $limit);
		$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
		$target = "$filenya?crkd=$crkd&crtipe=$crtipe&kunci=$kunci";
		$pagelist = $p->pageList($_GET['page'], $pages, $target);
		$data = mysqli_fetch_array($result);
		}


	else
		{
		//query
		$p = new Pager();
		$start = $p->findStart($limit);

		$sqlcount = "SELECT * FROM member_order ".
						"WHERE proses_status = 'true' ".
						"AND diterima_status = 'false' ".
						"ORDER BY booking_postdate DESC";
		$sqlresult = $sqlcount;

		$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
		$pages = $p->findPages($count, $limit);
		$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
		$pagelist = $p->pageList($_GET['page'], $pages, $target);
		$data = mysqli_fetch_array($result);
		}

	if ($count != 0)
		{
		//total nominal
		$qtyk = mysqli_query($koneksi, "SELECT SUM(subtotal) AS totalnya ".
											"FROM member_order ".
											"WHERE proses_status = 'true' ".
											"AND diterima_status = 'false'");
		$rtyk = mysqli_fetch_assoc($qtyk);
		$e_totalnya = nosql($rtyk['totalnya']);
					
					
				
			
		//view data				  
		echo '<br>
		[<b><font color="green">'.$count.'</font></b> Sedang Diproses]. 
		[Total Nominal : <b><font color="green">'.xduit3($e_totalnya).'</font></b>].
		<div class="table-responsive">          
		  <table class="table" border="1">
		    <thead>
				<tr bgcolor="'.$warnaheader.'">
				<th>BOOKING</th>
		        <th>MEMBER</th>
		        <th>ITEM</th>
		        <th>TOTAL ITEM</th>
		        <th>TOTAL QTY</th>
		        <th>TOTAL BERAT</th>
		        <th>TOTAL BAYAR</th>
		      </tr>
		    </thead>
		    <tbody>';



		do
			{
			if ($warna_set ==0)
				{
				$warna = $warna01;
				$warna_set = 1;
				}
			else
				{
				$warna = $warna02;
				$warna_set = 0;
				}

			//nilai
			$nomer = $nomer + 1;
			$kd = nosql($data['kd']);
			$i_member_kd = nosql($data['member_kd']);
			$i_member_nama = balikin($data['member_nama']);
			$i_nota = balikin($data['booking_kode']);
			$i_jml_jenis = balikin($data['barang_jml_jenis']);
			$i_jml_qty = balikin($data['barang_qty']);
			$i_jml_berat = balikin($data['barang_berat']);
			$i_member_nama = balikin($data['member_nama']);
			$i_booking = balikin($data['booking_postdate']);
			$i_tgl_bayar = balikin($data['bayar_postdate']);
			$i_total = balikin($data['subtotal']);



			echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
			echo '<td>
			<p>
			'.$i_tgl_bayar.'
			</p>

			<br>
			Kode Booking :
			<br>
			'.$i_nota.'
			
			<br>
			<a href="'.$filenya.'?s=selesai&kd='.$kd.'" class="btn btn-block btn-danger">SELESAI DAN DITERIMA</a>
			</td>
			<td>'.$i_member_nama.'</td>
			<td>';
			

			//query
			$q = mysqli_query($koneksi, "SELECT * FROM member_order_detail ".
											"WHERE member_kd = '$i_member_kd' ".
											"AND booking_kd = '$kd' ".
											"ORDER BY item_nama ASC");
			$row = mysqli_fetch_assoc($q);
			$total = mysqli_num_rows($q);
		

			echo '<table class="table" border="1">
					<tr bgcolor="'.$warnaheader.'">
		          <td>NAMA</td>
		          <td>KONDISI</td>
		          <td>BERAT</td>
		          <td>JUMLAH</td>
		        </tr>';
		
		
		
				do 
					{
					$r_kd = nosql($row['kd']);
					$r_itemkd = nosql($row['item_kd']);
					$r_nama = balikin($row['item_nama']);
					$r_kondisi = balikin($row['item_kondisi']);
					$r_berat = balikin($row['item_berat']);
					$r_filex = balikin($row['item_filex1']);
					$r_harga = nosql($row['item_harga']);
					$r_qty = nosql($row['jumlah']);
					$r_subtotal = nosql($row['subtotal']);
						
					if ($warna_set ==0)
						{
						$warna = $warna01;
						$warna_set = 1;
						}
					else
						{
						$warna = $warna02;
						$warna_set = 0;
						}
			
							
						//stock yang ada
						$qtyk = mysqli_query($koneksi, "SELECT * FROM m_item ".
															"WHERE kd = '$r_itemkd'");
						$rtyk = mysqli_fetch_assoc($qtyk);
						$e_jml = nosql($rtyk['jml']);
						
				
						echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
						echo '<td width="150">
						<img src="'.$sumber.'/filebox/item/'.$r_itemkd.'/'.$r_filex.'" width="150">
						<br>
						'.$r_nama.'
						<hr>
						'.xduit3($r_harga).'
						</td>
						<td width="100">'.$r_kondisi.'</td>
						<td width="100">'.$r_berat.' gram</td>
						<td width="50">'.$r_qty.'</td>
				        </tr>';
						}
					while ($row = mysqli_fetch_assoc($q));
			
					echo '</table>';


			echo '</td>
			<td>'.$i_jml_jenis.'</td>
			<td>'.$i_jml_qty.'</td>
			<td>'.$i_jml_berat.' Gram</td>
			<td>'.xduit3($i_total).'</td>
	    	</tr>';
			}
		while ($data = mysqli_fetch_assoc($result));

		echo '</tbody>
		  </table>
		  </div>

		<table width="100%" border="0" cellspacing="0" cellpadding="3">
		<tr>
		<td><strong><font color="#FF0000">'.$count.'</font></strong> Data. '.$pagelist.'</td>
		</tr>
		</table>';
		}
	else
		{
		echo '<p>
		<font color="red">
		<strong>TIDAK ADA DATA.</strong>
		</font>
		</p>';
		}
	}




echo '</form>
<br>
<br>
<br>';
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//isi
$isi = ob_get_contents();
ob_end_clean();

require("../../inc/niltpl.php");


//diskonek
xfree($qbw);
xclose($koneksi);
exit();
?>