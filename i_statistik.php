<?php
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
/////// KANTIN-SEKOLAH v1.0                                     ///////
///////////////////////////////////////////////////////////////////////
/////// Dibuat oleh :                                           ///////
/////// Agus Muhajir, S.Kom                                     ///////
/////// URL 	:                                               ///////
///////     * http://github.com/hajirodeon/                     ///////
///////     * http://sisfokol.wordpress.com/                    ///////
///////     * http://hajirodeon.wordpress.com/                  ///////
///////     * http://yahoogroup.com/groups/sisfokol/            ///////
///////     * https://www.youtube.com/@hajirodeon               ///////
/////// E-Mail	:                                               ///////
///////     * hajirodeon@yahoo.com                              ///////
///////     * hajirodeon@gmail.com                              ///////
/////// HP/SMS/WA : 081-829-88-54                               ///////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////




//query
$qtyk = mysqli_query($koneksi, "SELECT * FROM m_item");
$rtyk = mysqli_fetch_assoc($qtyk);
$jml_item = mysqli_num_rows($qtyk);



//query
$qtyk = mysqli_query($koneksi, "SELECT SUM(jml) AS total ".
											"FROM m_item");
$rtyk = mysqli_fetch_assoc($qtyk);
$jml_stock = nosql($rtyk['total']);



//query
$qtyk = mysqli_query($koneksi, "SELECT * FROM member_order");
$rtyk = mysqli_fetch_assoc($qtyk);
$jml_order = mysqli_num_rows($qtyk);




//query
$qtyk = mysqli_query($koneksi, "SELECT * FROM member_order ".
											"WHERE proses_status = 'false'");
$rtyk = mysqli_fetch_assoc($qtyk);
$jml_p_baru = mysqli_num_rows($qtyk);





//query
$qtyk = mysqli_query($koneksi, "SELECT * FROM member_order ".
											"WHERE proses_status = 'true' ".
											"AND diterima_status = 'false'");
$rtyk = mysqli_fetch_assoc($qtyk);
$jml_p_proses = mysqli_num_rows($qtyk);






//query
$qtyk = mysqli_query($koneksi, "SELECT * FROM member_order ".
											"WHERE diterima_status = 'true'");
$rtyk = mysqli_fetch_assoc($qtyk);
$jml_p_selesai = mysqli_num_rows($qtyk);








echo '<div class="card card-primary card-tabs">
  <div class="card-header p-0 pt-1">
    <ul class="nav nav-tabs" id="custom-tabs-one1-tab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="custom-tabs-one1-home-tab" data-toggle="pill" href="#custom-tabs-one1-home" role="tab" aria-controls="custom-tabs-one1-home" aria-selected="true">STATISTIK</a>
      </li>
    </ul>
  </div>
  <div class="card-body">
    <div class="tab-content" id="custom-tabs-one1-tabContent">
      <div class="tab-pane show active" id="custom-tabs-one1-home" role="tabpanel" aria-labelledby="custom-tabs-one1-home-tab">
		
		<ul>
    	<li><a href="#">Item Produk <span class="pull-right badge bg-blue">'.$jml_item.'</span></a></li>
        <li><a href="#">Stock <span class="pull-right badge bg-aqua">'.$jml_stock.'</span></a></li>
        <li><a href="#">Total Transaksi <span class="pull-right badge bg-green">'.$jml_order.'</span></a></li>
        
        <li><a href="#">Transaksi Baru <span class="pull-right badge bg-orange">'.$jml_p_baru.'</span></a></li>
        <li><a href="#">Transaksi Proses <span class="pull-right badge bg-orange">'.$jml_p_proses.'</span></a></li>
        <li><a href="#">Transaksi Selesai <span class="pull-right badge bg-purple">'.$jml_p_selesai.'</span></a></li>
        </ul>
						
			</div>

    </div>
  </div>
</div>

</div>';



