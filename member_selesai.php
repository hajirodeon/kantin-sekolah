<?php
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
/////// KANTIN-SEKOLAH v1.0                                     ///////
///////////////////////////////////////////////////////////////////////
/////// Dibuat oleh :                                           ///////
/////// Agus Muhajir, S.Kom                                     ///////
/////// URL 	:                                               ///////
///////     * http://github.com/hajirodeon/                     ///////
///////     * http://sisfokol.wordpress.com/                    ///////
///////     * http://hajirodeon.wordpress.com/                  ///////
///////     * http://yahoogroup.com/groups/sisfokol/            ///////
///////     * https://www.youtube.com/@hajirodeon               ///////
/////// E-Mail	:                                               ///////
///////     * hajirodeon@yahoo.com                              ///////
///////     * hajirodeon@gmail.com                              ///////
/////// HP/SMS/WA : 081-829-88-54                               ///////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////


session_start();


//ambil nilai
require("inc/config.php");
require("inc/fungsi.php");
require("inc/koneksi.php");
require("inc/cek/member.php");
$tpl = LoadTpl("template/cp_selesai.html");



nocache;

//nilai
$filenya = "member_selesai.php";
$judul = "Transaksi Kamu";
$judulku = $judul;
$s = nosql($_REQUEST['s']);
$notakd = nosql($_SESSION['notakd']);
$sesikd = nosql($_SESSION['kd6_session']);
$sesinama = cegah($_SESSION['nama6_session']);








//isi *START
ob_start();


require("template/js/number.js");
?>

<script language='javascript'>
//membuat document jquery
$(document).ready(function(){



	$("#btnKRM").on('click', function(){
		
		$("#formx2").submit(function(){
			$.ajax({
				url: "i_member_selesai.php?aksi=simpan",
				type:$(this).attr("method"),
				data:$(this).serialize(),
				success:function(data){					
					$("#ihasil").html(data);
					}
				});
			return false;
		});
	
	
	});	



			


		
});

</script>
	

<?php
//ketahui nota terakhir, yang belum dibayar
$qku = mysqli_query($koneksi, "SELECT * FROM member_order ".
								"WHERE member_kd = '$sesikd' ".
								"AND kd = '$notakd'");
$rku = mysqli_fetch_assoc($qku);
$tku = mysqli_num_rows($qku);
$ku_nota_kode = balikin($rku['booking_kode']);
$ku_tgl_booking = balikin($rku['booking_postdate']);
$ku_barang_jml_jenis = nosql($rku['barang_jml_jenis']);
$ku_barang_qty = nosql($rku['barang_qty']);
$ku_barang_berat = nosql($rku['barang_berat']);
$ku_subtotal = nosql($rku['subtotal']);
$ku_total = nosql($rku['total']);




//jika selesai ///////////////////////////////////////////////////////////////////////////////////////
if ($s == "selesai")
	{
	echo '<div class="alert alert-warning alert-dismissible">
      <h5><i class="icon fas fa-check"></i> Transaksi Pembelian Berhasil.</h5>
      	Silahkan Cek History Transaksi...
	<br>

	<a href="member_profil.php" class="btn btn-block btn-danger">OK >></a>

    </div>';
		
		
	//hapus sesi notakd
	$_SESSION['notakd'] = "";	
	}
else
	{
	//jika ada
	if (!empty($tku))
		{
		//ketahui sisa saldo
		//total kredit	
		$qku = mysqli_query($koneksi, "SELECT SUM(nominal) AS totalnya ".
										"FROM member_saldo ".
										"WHERE member_kd = '$sesikd' ".
										"AND jenis = 'KREDIT'");
		$rku = mysqli_fetch_assoc($qku);
		$ku_kreditnya = balikin($rku['totalnya']);
		
		
		//total debet
		$qku = mysqli_query($koneksi, "SELECT SUM(nominal) AS totalnya ".
										"FROM member_saldo ".
										"WHERE member_kd = '$sesikd' ".
										"AND jenis = 'DEBET'");
		$rku = mysqli_fetch_assoc($qku);
		$ku_debetnya = balikin($rku['totalnya']);
			
			
		$ku_sisa = $ku_debetnya - $ku_kreditnya;


		echo '<div class="info-box">
              <span class="info-box-icon bg-warning"><i class="fa fa-money"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Sisa Saldo</span>
                <span class="info-box-number">'.xduit3($ku_sisa).'</span>
              </div>
        </div>';
            

			
		//update total
		$q2 = mysqli_query($koneksi, "SELECT SUM(subtotal) AS total ".
										"FROM member_order_detail ".
										"WHERE member_kd = '$sesikd' ".
										"AND booking_kd = '$notakd'");
		$row2 = mysqli_fetch_assoc($q2);
		$totalnya2 = nosql($row2['total']);
		$totalnyax2 = xduit3($totalnya2);


		//hitung persen
		$nilku = round(($totalnya2 / $ku_sisa) * 100,2);
		
		
		
		//jika cukup
		if ($totalnya2 < $ku_sisa)
			{
			echo '<div class="info-box bg-success">
              <span class="info-box-icon"><i class="far fa-thumbs-up"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Saldo Cukup, Untuk Transaksi ini : </span>
                <span class="info-box-number">'.$totalnyax2.'</span>

                <div class="progress">
                  <div class="progress-bar" style="width: '.$nilku.'%"></div>
                </div>
                <span class="progress-description">
                  '.$nilku.'% dari Sisa Saldo
                </span>
              </div>
            </div>';
			}
		else
			{
			echo '<div class="info-box bg-danger">
              <span class="info-box-icon"><i class="far fa-thumbs-down"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">SALDO TIDAK CUKUP, Untuk Transaksi ini : </span>
                <span class="info-box-number">'.$totalnyax2.'</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 100%"></div>
                </div>
                <span class="progress-description">
                  Tidak Mencukupi...
                </span>
              </div>
            </div>';
			}
				
		
		
		echo '<form name="formx2" id="formx2">

		<h3>
		NO.NOTA : 
		<br>
		<b>'.$ku_nota_kode.'</b>
		</h3>
		
		<h3>
		Jumlah Jenis Item : 
		<br>
		<b>'.$ku_barang_jml_jenis.'</b>
		</h3>
		
		<h3>
		Jumlah Qty Item : 
		<br>
		<b>'.$ku_barang_qty.'</b>
		</h3>
		
		<h3>
		Total Berat : 
		<br>
		<b>'.$ku_barang_berat.' Gram</b>
		</h3>

	
		<hr>

	
		
		<hr>				
			<p>
			<input name="sesikd" id="sesikd" type="hidden" value="'.$sesikd.'">
			<input name="notakd" id="notakd" type="hidden" value="'.$notakd.'">
			<input name="totalnya" id="totalnya" type="hidden" value="'.$totalnya2.'">
			<input name="btnKRM" id="btnKRM" type="submit" class="btn btn-block btn-danger" value="BAYAR >>">
			</p>
		<hr>';



		//query
		$q = mysqli_query($koneksi, "SELECT * FROM member_order_detail ".
										"WHERE member_kd = '$sesikd' ".
										"AND booking_kd = '$notakd' ".
										"ORDER BY item_nama ASC");
		$row = mysqli_fetch_assoc($q);
		$total = mysqli_num_rows($q);
			
	
	
		?>
		
		
		  
		  <script>
		  	$(document).ready(function() {
		    $('#table-responsive').dataTable( {
		        "scrollX": true
		    } );
		} );
		  </script>
		  
		<?php
	
	
		echo '<div class="table-responsive">          
			  <table class="table" border="1">
			    <thead>
				<tr bgcolor="'.$warnaheader.'">
	          <th>PRODUK</th>
	          <th>JUMLAH</th>
	          <th>SUBTOTAL</th>
	        </tr>
			    </thead>
			    <tbody>';
	
	
	
		do 
			{
			$r_kd = nosql($row['kd']);
			$r_itemkd = nosql($row['item_kd']);
			$r_nama = balikin($row['item_nama']);
			$r_kondisi = balikin($row['item_kondisi']);
			$r_berat = balikin($row['item_berat']);
			$r_filex = balikin($row['item_filex1']);
			$r_harga = nosql($row['item_harga']);
			$r_qty = nosql($row['jumlah']);
			$r_subtotal = nosql($row['subtotal']);
				
			if ($warna_set ==0)
				{
				$warna = $warna01;
				$warna_set = 1;
				}
			else
				{
				$warna = $warna02;
				$warna_set = 0;
				}
	
					
				//stock yang ada
				$qtyk = mysqli_query($koneksi, "SELECT * FROM m_item ".
										"WHERE kd = '$r_itemkd'");
				$rtyk = mysqli_fetch_assoc($qtyk);
				$e_jml = nosql($rtyk['jml']);
				
		
				echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
				echo '<td width="150">
				<img src="'.$sumber.'/filebox/item/'.$r_itemkd.'/'.$r_filex.'" width="150">
				<br>
				'.$r_nama.'
				<br>
				'.$r_kondisi.'
				<br>
				'.$r_berat.' gram
				<br>
				'.xduit3($r_harga).'</td>
				<td width="50">'.$r_qty.'</td>
				<td width="150" align="right">'.xduit3($r_subtotal).'</td>
		        </tr>';
				}
			while ($row = mysqli_fetch_assoc($q));
	
			echo '</tbody>
			  </table>
			  </div>';
		}
		
	else
		{
		echo '<font color=red>
		<h3>Maaf, Silahkan Belanja Dahulu...</h3>
		</font>';
		}
	
	
	
	echo '</form>
	
	
		<div id="ihasil"></div>';
	}




//isi
$isi = ob_get_contents();
ob_end_clean();










require("inc/niltpl.php");


//diskonek
xclose($koneksi);
exit();
?>